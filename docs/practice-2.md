# Practice 2

## Test Simple Calculator

Kita akan membuat unit test untuk class Calculator. Asumsi sudah menginstall laravel/clone dari repository git yang disediakan.

### RED

- Pertama kita akan melakukan test yang hasilnya RED (failed)
- Pindah ke directory project laravel yang telah dibuat, lalu ketik pada console: 
```bash
php artisan make:test --unit CalculatorTest
```
- Buka file `tests/Unit/CalculatorTest.php` yang telah dibuat, lalu edit menjadi:

```php
<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\Calculator;

class CalculatorTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testAdd()
    {
        // Arrange
        $calculator = new Calculator();
        
        // Act
        $result     = $calculator->add(1, 2);
        
        // Assert
        $this->assertEquals(3, $result);
    }
}
```

- Artinya kita akan melakukan unit test pada class `App\Services\Calculator`
- Yang ditest adalah function `add` yang berfungsi untuk melakukan penjumlahan angka
- Mulai test dengan menjalankan perintah pada console: 

```bash
vendor/bin/phpunit
```

- Akan muncul error: `Error: Class 'App\Services\Calculator' not found` karena class belum dibuat ![Failed](img/failed-1.png)
- Dari gambar dapat dilihat ada tiga titik dan satu huruf `.E..` ini artinya ada 4 test case, yang mana hasilnya: 3 berhasil, dan 1 error (E = Error)

### GREEN

- Hasil yang RED tadi harus kita buat GREEN (success)
- Buat folder dan file: `App\Services\Calculator.php`, lalu isi dengan:


```php
<?php

namespace App\Services;

class Calculator
{

    function add($a, $b)
    {
        return 3;
    }
}
```
- Jalankan phpunit ![Success](img/success-1.png)
- Test akan passed. Tetapi code ini bermasalah: karena hasilnya di-hardcode
- Coba kita tambah test case pada file `CalculatorTest`:

```php
...
    public function testAddMore()
    {
        $calculator = new Calculator();
        $result     = $calculator->add(3, 4);
        $this->assertEquals(7, $result);
    }
...
```
- Jalankan lagi phpunit
- Akan muncul error `Failed asserting that 3 matches expected 7` karena hasilnya seharusnya adalah 7, tetapi yang keluar ternyata 3: ![Failed](img/failed-2.png)
- Dari gambar dapat dilihat ada 4 titik dan satu huruf `..F..` ini artinya ada 4 test case yang sukses dan satu yang gagal (F = Failed)
- Keterangan huruf yang ada pada output phpunit:
    - . Printed when the test succeeds.
    - F Printed when an assertion fails while running the test method.
    - E Printed when an error occurs while running the test method.
    - R Printed when the test has been marked as risky (see Chapter 6).
    - S Printed when the test has been skipped (see Chapter 7).
    - I Printed when the test is marked as being incomplete or not yet implemented (see Chapter 7).


### REFACTOR

- Mari kita refactor function `add` nya menjadi:

```php
...

    function add($a, $b)
    {
        return $a + $b;
    }
    
...
```

- Lalu jalankan test lagi: ![Success](img/success-2.png)
- Yeay, we did it!
- Coba kita tambah test case lagi agar lebih yakin dengan function yang kita buat:

```php
...

    public function testAddNegative()
    {
        $calculator = new Calculator();
        $result     = $calculator->add(-1, -1);
        $this->assertEquals(-2, $result);
    }

    public function testAddNegative2()
    {
        $calculator = new Calculator();
        $result     = $calculator->add(-10, 2);
        $this->assertEquals(-8, $result);
    }

    public function testAddNegative3()
    {
        $calculator = new Calculator();
        $result     = $calculator->add(4, -10);
        $this->assertEquals(-6, $result);
    }

...   
```

- Lalu jalankan test lagi: ![Success](img/success-3.png)

### Next Iteration of Red-Green-Refactor

Mungkin saja consumer dari class `Calculator` ada yang memasukan tipe bukan angka, contoh string, boolean, object. Apa yang akan terjadi? hasilnya tidak dapat diprediksi. Agar class yang kita buat lebih robust, mari kita buat spesifikasi untuk parameter yang bukan angka, maka akan kita tolak dengan mengembalikan nilai false.

- Buat test case baru:

```php

    public function testAddWithWrongParameter()
    {
        $calculator = new Calculator();
        $result     = $calculator->add("", "");
        $this->assertEquals(false, $result);
    }
    
```
- Jalankan test ![Failed](img/failed-3.png)
- Hasilnya ternyata muncul Exception `ErrorException: A non-numeric value encountered`, mari kita buat hijau dengan me-refactor file `Calculator` menjadi:
```php
...
    function add($a, $b)
    {
        if (!is_numeric($a) || !is_numeric($b)) {
            return false;
        }

        return $a + $b;
    }
...
```

- Jalankan phpunit lagi ![Success](img/success-4.png)
- Yeay! ![Alhamdulillah](img/alhamdulillah.gif)

Test case dapat ditambahkan untuk kasus lain yang mungkin akan terjadi. Test case utama bisa didapat dari requirement, test case tambahannya dapat ditambah sesuai penemuan pada waktu proses coding.

Begitulah kurang lebih proses TDD, lifecycle Red-Green-Refactor akan membuat software yang kita buat menjadi lebih robust, spesifikasi software pun terdokumentasi dengan lengkap pada test case yang dibuat. Semakin banyak test case yang dibuat, semakin 'aman' program yang kita buat. Error akan tertangkap di awal proses development, sebelum program berjalan di staging & production. 

Jika ada yang merubah code `Calculator` tanpa menulis unit test nya, maka akan diketahui dengan menjalankan phpunit akan mengeluarkan error karena tidak sesuai spesifikasi.

Pembahasan di sini sangat basic. Diharapkan temen-temen meng-explore lebih lanjut mengenai TDD seperti mock, integration test, browser test, dll. 

Selamat bereksplorasi!

## Test Billing 

### Unit Test

Kita akan membuat unit test untuk class Billing.

- Buat unit test `BillingTest`
```bash
php artisan make:test --unit BillingTest
```
- Buat test case untuk rating motor

```php
use App\Services\Billing;
use Mockery as m;
use App\Repositories\RateRepositoryInterface;
...
    function testMotor()
    {
        // Arrange
        $rates = [
            (object) ['vehicle_class_id' => 1, 'price' => 10000,],
            (object) ['vehicle_class_id' => 2, 'price' => 25000,],
            (object) ['vehicle_class_id' => 3, 'price' => 30000,],
            (object) ['vehicle_class_id' => 4, 'price' => 50000,],
        ];
        
        $rateRepo = m::mock(RateRepositoryInterface::class);
        $rateRepo->shouldReceive('all')->andReturn($rates);

        $billing               = new Billing($rateRepo);
        $passage               = new \stdClass();
        $passage->vehicle_class = 1;

        // Act
        $rate = $billing->rate($passage);

        // Assert
        $this->assertEquals(10000, $rate);
    }
```

- Karena pada latihan sebelumnya sudah dibuat class Billing dan dilihat dari struktur classnya sepertinya sudah bisa ditest, coba jalankan phpunit
- Penjelasan dari test case diatas:
    - Karena ini merupakan unit test, sehingga hubungan dengan modul/class lain harus di-mock 
    - object $rates adalah data fixture yang ditentukan sebagai konfigurasi rate
    - $rateRepo adalah mock dari RateRepositoryInterface
    - $rateRepo dapat dipanggil dari dalam class Billing dengan mengembalikan data yang sebelumnya ditentukan, sehingga tidak memanggil ke database

### Integration Test

Kita akan membuat integration test (mengakses database)

- Ketik pada console:

```bash
php artisan make:test BillingTest # perhatikan tidak menggunakan --unit
```
- Buka file `tests/Feature/BillingTest.php` yang telah dibuat, lalu edit menjadi:

```php

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\RateRepositoryInterface;
use App\Services\Billing;

class BillingTest extends TestCase
{

    function testMotor()
    {
        // Arrange
        $rates = [
            ['vehicle_class_id' => 1, 'price' => 10000, 'lane' => 1],
            ['vehicle_class_id' => 2, 'price' => 25000, 'lane' => 1],
            ['vehicle_class_id' => 3, 'price' => 30000, 'lane' => 1],
            ['vehicle_class_id' => 4, 'price' => 50000, 'lane' => 1],
        ];

        $rateRepo = app(RateRepositoryInterface::class);
        foreach ($rates as $rate) {
            $rateRepo->create($rate);
        }

        $billing                = new Billing($rateRepo);
        $passage                = new \stdClass();
        $passage->vehicle_class = 1;

        // Act
        $rate = $billing->rate($passage);

        // Assert
        $this->assertEquals(10000, $rate);
    }
}
```

- Coba jalankan phpunit ![Success](img/success-5.png)
- Test ini menggunakan database sqlite_memory (atau sesuai setting pada file .env.testing)
- Pada test ini $rateRepo harus meng-create data rates pada database, dan akan dipanggil dari dalam class Billing
- Setiap test dijalankan, database akan di-refresh
- Kita dapat juga melihat hasilnya dalam format testdox. Coba jalankan:
```bash
vendor/bin/phpunit --testdox
```
![Success](img/success-6.png)


## More Tests
- Tambah test case untuk kendaraan kecil, sedang dan besar
- Lanjutkan dengan menambah rating key lain, contoh lane
- Jangan lupa menggunakan TDD lifecycle, yaitu Red-Green-Refactor
- Jika masih ada waktu, coba TDD untuk bisnis proses top-up
- Coba juga Laravel Dusk

## Thank's

Terima kasih atas perhatian dan waktu dari temen-temen. Mudah-mudahan workshop ini ada manfaatnya dan bisa digunakan pada project-project kita selanjutnya. Memang TDD bukan hal yang mudah dan memerlukan usaha yang sungguh-sungguh. Tetapi jika dilakukan dengan rutin, insyaAllah lama-lama tidak terasa kita akan menjadi master of TDD. Sekali lagi terima kasih  dan sampai jumpa di lain kesempatan. Wassalamu'alaikum wr. wb.
