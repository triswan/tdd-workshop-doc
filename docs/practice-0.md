# Introduction

Berikut adalah list problem untuk masalah yang sering terjadi pada kode yang tidak 'clean'. Silakan dipelajari dan berikan solusi untuk masing2 problem. Solusi code tidak harus dapat dijalankan (dapat juga berbentuk pseudo-code), tetapi yang dinilai adalah cara berfikir/cara menyelesaikan masalah nya.

## Problem 1

- Story: Fungsi untuk menampilkan halaman (page) dan footer 
- Problem: Method/function terlalu panjang

```php
function show() {
    $page = $this->getPage();
    
    echo 'title: ' . $page->title . '<br>';
    echo 'date: ' . $page->date . '<br>';
    echo 'content: ' . $page->content . '<br>';
    
    $footer = $this->getFooter();
    
    echo $footer->left . ' ';
    echo $footer->center . ' ';
    echo $footer->right ;
}
```

Silakan tulis code solusinya

## Problem 2

- Problem: Terlalu banyak nested block

```php
if ($x == 1) {
    if ($y == 4) {
        if ($z == true) {
            if ($z == 12) {
                echo 'ok';
            }
        }
   } 
}
```

Silakan tulis code solusinya, mohon setiap error dilog

## Problem 3

- Story: Fungsi untuk menyimpan data order dan data detail dari pembelian oleh customer pada web e-commerce
- Problem: Tanggung jawab terlalu banyak dan bercampur dalam satu function

```php
function save() {
    $sales_code = 'SO/'.rand(1, 1000).date('Ym').'/XVII'.$this->input->post('customer_id');
    
    $customer_id = $this->session->data('customer_id');
    $payment_id = $this->input->post('payment_id');
    
    // insert master order
    $order_id = $this->db->query('insert into order(date, customer_id, payment_id) values (\''.date('Y-m-d').'\','. $customer_id.', '. $payment_id.') ')->run();
    
    // insert detail order
    foreach ($this->input->post('products') as $p) {
        $data = [
            'order_id' => $order_id,
            'price' => $p->price,
            'qty' => $p->qty,
            'status' => 2,
        ];
        $this->db->insert('order_details', $data );
        
    }
    
    $data['success'] = '<strong>berhasil</strong>';
    $this->load->view('header');
    $this->load->view('body', $data);
    $this->load->view('header');
}
```

Silakan tulis code solusinya

## Problem 4

- Story: Aplikasi yang menggunakan payment gateway untuk sistem pembayarannya
- Problem: Untuk mengganti payment gateway, harus mengedit program utama

```php

class Doku {
    
    function submit($payment) {
        echo 'sending payment to doku';
    }
    
}

class MainProgram {
    
    function pay() {
        $payment = $this->input->('payment_form');
        
        $doku = new Doku();
        $doku->submit($payment);
    }
    
}

```

Silakan tulis code solusinya

## Problem 5

- Story: Aplikasi yang dapat mengirim notifikasi ke user nya
- Problem: Untuk menambah/mengganti notifikasi, harus mengedit program utama

```php

class Email {
    
    function mail($address, $content) {
        echo 'sending email notification';
    }
    
}

class Sms {
    
    function send($sms, $content) {
        echo 'sending sms notification';
    }
    
}

class MainProgram {
    
    function notifyUsers() {
        $users = $this->model->getAllUsers();
        
        foreach ($users as $user) {
            $email = new Email();
            $email->mail($user->email, 'Notifikasi untuk user');
        }
    }
    
}

```

Silakan tulis code solusinya
