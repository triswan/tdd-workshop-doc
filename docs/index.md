# Index 

Berikut adalah beberapa list 'jurus' yang dapat diterapkan ketika me-refactor / membuat suatu program yang maintainable dan testable. Silakan pelajari.

## Extract Method

### Problem

- Terdapat code yang perlu di-grup

```php
function showUser() {
    $user = $this->model->getUserById(1);

    echo $user->name;
    echo $user->email;
    echo $user->phone;
}
```
### Solution

- Pindahkan grup code ke method baru, lalu panggil method tersebut
 
```php
function showUser() {
    $user = $this->model->getUserById(1);
    $this->printUser($user);
}

function printUser($user) {
    echo $user->name;
    echo $user->email;
    echo $user->phone;
}
```

## Extract Variable

### Problem

- Terdapat ekspresi yang kompleks sulit dipahami

```php
if (substr($platNomor, -2, 1) == 'T' || substr($platNomor, -2, 2) == 'UA') {
    // do something
}
```

### Solution

- Ekstrak ekspresi ke method tersendiri dan beri nama yang deskriptif

```php
if ($this->platNomorTaksi($platNomor)) {
    // do something
}

function platNomorTaksi($platNomor) {
    return substr($platNomor, -2, 1) == 'T' || substr($platNomor, -2, 2) == 'UA';
}
```

## Reuse

### Problem

- Terdapat local variable yang perlu di-reuse ti tempat lain

```php
function showInvoice() {
    $total = $this->price - $this->discount;
    echo 'Total: ' . $total;
}

function sendInvoice() {
    $total = $this->price - $this->discount;
    $this->email('foo@bar.com', $total);
}
```

### Solution

- Pindahkan ke method baru, dan return hasilnya

```php
function showInvoice() {
    echo 'Total ' . $this->countTotal();
}

function sendInvoice() {
    $this->email('foo@bar.com', $this->countTotal());
}

function countTotal() {
    return $this->price - $this->discount;
}
```

## Remove Duplicates

- Terdapat duplikasi yang tidak perlu
- Jika ada perubahan harus di-edit satu-satu


### Problem 

```php
if ($kondisi == true) {
    $var1 = 1;
    $var2 = 2;
    $this->operation1($var1, $var2);
} else {
    $var1 = 1;
    $var2 = 2;
    $this->operation2($var1, $var2);
}
```
### Solution

- Pindahkan duplikasi ke tempat yang dapat di-reuse

```php
$var1 = 1;
$var2 = 2;
if ($kondisi == true) {
    $this->operation1($var1, $var2);
} else {
    $this->operation2($var1, $var2);
}
```

## Replace Magic Numbers

### Problem

- Terdapat angka-angka yang tiba-tiba muncul dan hanya diketahui oleh penulisnya. Sering penulisnya sendiri lupa maksudnya apa.

```php
if ($userStatus == 3) {
    // do someThing
}
```
- Ganti dengan constant atau class constant

### Solution

```php
if ($userStatus == STATUS_ACTIVE) {
    // do someThing
}

// or 

if ($userStatus == User::STATUS_ACTIVE) {
    // do someThing
}
```

## Long parameter list

### Problem

- Terlalu banyak parameter dalam sebuah function
- Sulit untuk dibaca dan diingat
- Jika terdapat perubahan signature dapat menyebabkan error pada client yang memanggil

```php
function submit($firstName,$lastName,$email,$phone,$address,$website,$twitter,$facebook,$product_id) {
    //
}
```

### Solution

- Gunakan object / array untuk membungkus parameter yang panjang. Jika perlu dapat dikelompokan.

```php
function submit(User $user, $product_id) {
    //
}

// or 

function submit(array $user, $product_id) {
    //
}
```

## Use Early Return

### Problem

- Terdapat nested-blocks yang terlalu dalam
- Akan sulit di-test dan di-trace jika terjadi error
- Sulit di-maintain jika ada perubahan

```php
if ($accountFound == true) {
    if ($accountStatus == 2) {
        if ($enoughBalance == true) {
            return true;
        }
   } 
}
```

### Solution

- Gunakan kondisi lawannya, lalu segera return

```php
if ($accountFound != true) {
    return false;
} 

if ($accountStatus != ACCOUNT_STATUS_ACTIVE) {
    return false;
} 

if ($enoughBalance == true) {
    return true;
} 

return false;
```

## Use Intention-Revealing Names

- Terdapat nama variable / function yang kurang jelas maksudnya

### Problem

```php
$maH = 12; // Max hours
$miH = 5; // Min hours
```

### Solution

- Gunakan nama yang jelas sesuai maksud variable nya

```php
$maxHours = 12;
$minHours = 5;
```

## Make Meaningful Distinctions

### Problem

- Terdapat beberapa variable / function yang tidak jelas perbedaannya

```php
$cust
$customer
$customerInfo
$customerData
```
- Bedakan penamaan sesuai dengan maksudnya

### Solution

```php
$allCustomers
$activeCustomers

```

## Konsisten dalam penamaan

### Problem

- Terdapat istilah yang berbeda untuk maksud yang sama

```php
getUsers();
fetchUsers();
retrieveUsers();

InvoiceManager();
InvoiceController();
```

### Solution

- Gunakan istilah yang sama untuk maksud yang sama

```php
getUsers();

InvoiceManager();
```

## Pisahkan concern

### Problem

- Concerns bercampur dalam satu class/function
- Contoh concern data, concern tampilan, concern bisnis dalam satu function:

```php
function submitPassage($passage) {
    // concern: data access
    $account = $this->db->query('select * from accounts where nummber = ' . $passage->accountNumber)->result();
    
    // concern: bisnis
    if ($account->status == ACCOUNT_STATUS_ACTIVE) {
        $account->balance = $account->balance - $passage->price;
    }
    
    // concern: tampilan
    echo "<strong>{$account->name} berhasil membayar {$passage->id}</strong>";
}
```

### Solution

- Pisahkan concern ke masing-masing modul/class nya, lalu panggil dari function tersebut

```php
function submitPassage($passage) {
    $account = $this->accountModel->getByNumber($passage->accountNumber);
    
    $this->billing->charge($account, $passage);
    
    $this->view($account, $passage);
}
```

# SOLID

## SRP

### Problem

```php
class Report
{
    public function getTitle() 
    {
        return 'Report Title';
    }
    public function getDate() 
    {
        return '2016-04-21';
    }
    public function getContents() 
    {
        return [
            'title' => $this->getTitle(),
            'date' => $this->getDate(),
        ];
    }
    public function formatJson()
    {
        return json_encode($this->getContents());
    }
}
```

### Solution

```php
class Report
{
    public function getTitle() 
    {
        return 'Report Title';
    }
    public function getDate() 
    {
        return '2016-04-21';
    }
    public function getContents() 
    {
        return [
            'title' => $this->getTitle(),
            'date' => $this->getDate(),
        ];
    }
}

class JsonReportFormatter
{
    public function format(Report $report)
    {
        return json_encode($report->getContents());
    }
}

```

## OCP

### Problem

```php
class Programmer
{
    public function code()
    {
        return 'coding';
    }
}
class Tester
{
    public function test()
    {
        return 'testing';
    }
}
class ProjectManagement
{
    public function process($member)
    {
        if ($member instanceof Programmer) {
            $member->code();
        } elseif ($member instanceof Tester) {
            $member->test();
        };
        throw new Exception('Invalid input member');
    }
}

```

### Solution

```php
interface Workable
{
    public function work();
}
class Programmer implements Workable
{
    public function work()
    {
        return 'coding';
    }
}
class Tester implements Workable
{
    public function work()
    {
        return 'testing';
    }
}
class ProjectManagement
{
    public function process(Workable $member)
    {
        return $member->work();
    }
}

```

## LSP

```php
class Rectangle
{
    protected $width;
    protected $height;
    public function setHeight($height)
    {
        $this->height = $height;
    }
    public function getHeight()
    {
        return $this->height;
    }
    public function setWidth($width)
    {
        $this->width = $width;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function area()
    {
         return $this->height * $this->width;
    }
}
class Square extends Rectangle
{
    public function setHeight($value)
    {
        $this->width = $value;
        $this->height = $value;
    }
    public function setWidth($value)
    {
        $this->width = $value;
        $this->height = $value;
    }
}
class RectangleTest
{
    private $rectangle;
    public function __construct(Rectangle $rectangle)
    {
        $this->rectangle = $rectangle;
    }
    public function testArea()
    {
        $this->rectangle->setHeight(2);
        $this->rectangle->setWidth(3);
        // Expect rectangle's area to be 6
    }
}

```

### Solution

- Perhitungan square tidak match dengan rectangle, harus menggunakan area lain yang perhitungannya yang sama

## ISP

### Problem 

```php
interface Workable
{
    public function canCode();
    public function code();
    public function test();
}
class Programmer implements Workable
{
    public function canCode()
    {
        return true;
    }
    public function code()
    {
        return 'coding';
    }
    public function test()
    {
        return 'testing in localhost';
    }
}
class Tester implements Workable
{
    public function canCode()
    {
        return false;
    }
    public function code()
    {
         throw new Exception('Opps! I can not code');
    }
    public function test()
    {
        return 'testing in test server';
    }
}
class ProjectManagement
{
    public function processCode(Workable $member)
    {
        if ($member->canCode()) {
            $member->code();
        }
    }
}
```

### Solution

```php
interface Codeable
{
    public function code();
}
interface Testable
{
    public function test();
}
class Programmer implements Codeable, Testable
{
    public function code()
    {
        return 'coding';
    }
    public function test()
    {
        return 'testing in localhost';
    }
}
class Tester implements Testable
{
    public function test()
    {
        return 'testing in test server';
    }
}
class ProjectManagement
{
    public function processCode(Codeable $member)
    {
        $member->code();
    }
}
```

## DIP

### Problem

```php
class Mailer
{
}
class SendWelcomeMessage
{
    private $mailer;
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
}
```

### Solution

```php
interface Mailer
{
    public function send();
}
class SmtpMailer implements Mailer
{
    public function send()
    {
    }
}
class SendGridMailer implements Mailer
{
    public function send()
    {
    }
}
class SendWelcomeMessage
{
    private $mailer;
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
}
```