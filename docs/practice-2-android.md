# Practice 2 (Android)

## Test Simple Calculator

Kita akan membuat unit test untuk class Calculator. Asumsi sudah membuat android project/clone repository dari git yang disediakan.

### RED

- Pertama kita akan melakukan test yang hasilnya RED (failed)
- Buat file `CalculatorTest.java` pada package `id.co.swamedia.tddworkshop (test)`, lalu isi dengan code

```java
package id.co.swamedia.tddworkshop;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    @Test
    public void calculator_AddOneTwo_ReturnsThree() {
        Calculator calculator = new Calculator();
        int result = calculator.add(1,2);
        assertEquals(3, result);
    }
}

```
- Artinya kita akan melakukan unit test pada class `id.co.swamedia.tddworkshop.Calculator`
- Yang ditest adalah function `add` yang berfungsi untuk melakukan penjumlahan angka
- Jika `assertEquals` tidak ditemukan, klik `Alt+Enter` lalu pilih `Import static method...`

![](img/android-1.png)

Coba run test: Pilih file test tersebut, klik kanan lalu pilih Run 'CalculatorTest' (Ctrl+Shift+F10)

![](img/android-2.png)

Maka Android Studio akan menampilkan error `error: cannot find symbol class Calculator` karena classnya belum dibuat

![](img/android-3.png)

### GREEN

- Hasil yang RED tadi harus kita buat GREEN (success)
- Buat file `Calculator.java` pada package `id.co.swamedia.tddworkshop` lalu isi dengan:

```java
package id.co.swamedia.tddworkshop;

public class Calculator {
    public int add(int num1, int num2) {
        return 3;
    }
}

```

Maka error pada file `CalculatorTest.java` akan hilang

![](img/android-4.png)

Coba jalankan test lagi

![](img/android-5.png)

- Test akan lulus. Tetapi code ini bermasalah: karena hasilnya di-hardcode
- Coba kita tambah test case pada file `CalculatorTest`

```java
...
    @Test
    public void calculator_AddThreeFour_ReturnsSeven() {
        Calculator calculator = new Calculator();
        int result = calculator.add(3,4);
        assertEquals(7, result);
    }
...    
```
- Jalankan lagi test
- Akan muncul error

```java
java.lang.AssertionError: 
Expected :7
Actual   :3
```

![](img/android-6.png)

### REFACTOR

- Mari kita refactor function add nya menjadi:

```java
...
    public int add(int num1, int num2) {
        return num1 + num2;
    }
...
```

- Lalu jalankan test lagi

![](img/android-7.png)

- Yeay, we did it!
- Coba kita tambah test case lagi agar lebih yakin dengan function yang kita buat:

```java
...
    @Test
    public void calculator_AddMinusOneMinusOne_ReturnsMinusTwo() {
        Calculator calculator = new Calculator();
        int result = calculator.add(-1, -1);
        assertEquals(-2, result);
    }
    
    @Test
    public void calculator_AddMinusTenTwo_ReturnsMinusEight() {
        Calculator calculator = new Calculator();
        int result = calculator.add(-10, 2);
        assertEquals(-8, result);
    }

    @Test
    public void calculator_AddFourMinusTen_ReturnsMinusSix() {
        Calculator calculator = new Calculator();
        int result = calculator.add(4, -10);
        assertEquals(-6, result);
    }
...    
```

![](img/android-8.png)

- Yeay
- Coba case lain yang mungkin terjadi, contoh overflow

```java
...
    @Test
    public void calculator_AddOverflowValue_ReturnsMinus() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2147483647, 1);
        assertEquals(-2147483648, result);
    }
...
```

- Jalankan test

![](img/android-9.png)

Test case dapat ditambahkan untuk kasus lain yang mungkin akan terjadi. Test case utama bisa didapat dari requirement, test case tambahannya dapat ditambah sesuai penemuan pada waktu proses coding.

Begitulah kurang lebih proses TDD, lifecycle Red-Green-Refactor akan membuat software yang kita buat menjadi lebih robust, spesifikasi software pun terdokumentasi dengan lengkap pada test case yang dibuat. Semakin banyak test case yang dibuat, semakin 'aman' program yang kita buat. Error akan tertangkap di awal proses development, sebelum program berjalan di staging & production.

Jika ada yang merubah code Calculator tanpa menulis unit test nya, maka akan diketahui dengan menjalankan phpunit akan mengeluarkan error karena tidak sesuai spesifikasi.

Pembahasan di sini sangat basic. Diharapkan temen-temen meng-explore lebih lanjut mengenai TDD seperti mock, integration test, instrumentation test, dll.

Selamat bereksplorasi!


