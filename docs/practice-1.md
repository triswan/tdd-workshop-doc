# Introduction

Practice ini adalah untuk membuat sebuah aplikasi yang 'clean' dan dapat di-test secara otomatis yaitu unit testing. Akan dijelaskan langkah-langkah yang biasa dilakukan jika tidak mengugunakan unit testing. Hasil source code nya akan sulit di-maintain dan di-test. Lalu dijelaskan langkah untuk me-refactor menjadi lebih 'clean' dan mudah di-test.

# Requirements

## Business Process

![JBE](img/jbe.png)

### Passage

![Bispro Passage](img/bispro-passage-a.png)

### Top up

![Bispro Topup](img/bispro-topup-a.png)

## Terms

|EN|ID| Keterangan
|---|---|---
|Balance|Saldo|Saldo e-wallet pemilik akun
|Enforcement|Pelanggaran|Pelanggaran kendaraan yang melintas JBE
|ERP|JBE|Electronic Road Pricing/Jalan Berbayar Elektronik
|Fine|Denda|Jumlah denda yang dikenakan karena pelanggaran
|License Plate|Plat Nomor|Plat nomor kendaraan
|OBU|IKE|On Board Unit/Identitas Kendaraan Elektronik, Alat yang dipasang pada kendaraan sebagai STNK elektronik
|Passage|Passage|Kendaraan yang melintas
|Rate|Biaya|Biaya melintas JBE
|Top up|Isi ulang|Isi ulang saldo e-wallet
|Vehicle Class|Golongan Kendaraan|Golongan kendaraan, misalnya motor, kendaraan kecil, kendaraan sedang, kendaraan besar

## Domain Model

![Domain Model](img/domain-model-a.png)

## Use Case

![Use Case](img/use-case-a.png)

## Passage Activity

![Passage Activity](img/act-passage.png)

## Top up Activity

![Top Up Activity](img/act-topup.png)

 
# "The Usual Way"

Biasanya ketika kita ngoding, prosesnya kurang lebih seperti berikut:

(Asumsi sudah menginstall laravel/clone dari repository git yang disediakan)

## Step 1: Insert Passage

- Kita akan membuat fitur passage yang dapat di-call oleh sistem roadside
- Pindah ke directory laravel, lalu ketik pada console:

```bash
php artisan make:controller PassageController
```
- Buka file `App\Controllers\PassageController`
- Ketik code untuk meng-insert passage ke database menggunakan Eloquent:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Passage;
use App\VehicleClass;

class PassageController extends Controller
{

    // menerima data kendaraan lewat
    function receive(Request $request) {
        
        $ps = new Passage();
        $ps->price = 10000; // WARNING: hardcode
        $ps->account_id = Account::where('number', $request->account_number)->where('status_id', 1)->first()->id;
        $ps->vehicle_class_id = VehicleClass::where('code', $request->vehicle_class)->first()->id;
        $ps->license_plate = $request->license_plate_number;
        $ps->lane = $request->lane;
        $ps->save();
    
        echo 'success';
        
    }

}

```

- Jangan lupa sesuaikan route nya
- Coba test dengan tool API tester, contoh Postman 
    - Method: POST 
    - Url: /passage/receive
    - Body: form-data
        - account_number: (lihat di database)
        - vehicle_class: 001
        - license_plate_number: B123C
        - lane: 1
- Jika memerlukan data seed, coba jalankan
```
php artisan db:seed
```
- Data seed akan diinsert ke database sebanyak 50
 
## Step 2: Tambah Pengecekan Account

```php
...

    function receive(Request $request) {
        $ps = new Passage();

        // Tambah pengecekan account dan vehicle class biar ngga error kalo data ngga ketemu plus nanti bisa dirating accountnya
        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();
        
        if ($ac != '') {
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }
            $ps->price = 10000;
            $ps->account_id = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate = $request->license_plate_number;
            $ps->lane = $request->lane;
            $ps->save();

            echo 'success';
        } else {
            echo 'failed';
        }
        
    }
    
...
```

## Step 3: Tambah Rating


```php
...
    
    function receive(Request $request) {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        if ($ac != '') {
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }
            
            $ps->price = 10000;
            $ps->account_id = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate = $request->license_plate_number;
            $ps->lane = $request->lane;
            $ps->save();

            // rating
            $ac->balance = $ac->balance - 10000; 
            $ac->save();

            echo 'success';
        } else {
            echo 'failed';
        }
    }
    
...
```

## Step 4: Variasi Harga

```
...

    function receive(Request $request) {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        if ($ac != '') {
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }

            // Handle harga untuk variasi vehicle class
            if ($vcId == 1) {
                $harga = 10000;
            }
            else if ($vcId == 2) {
                $harga = 25000;
            }
            else if ($vcId == 3) {
                $harga = 30000;
            }
            else if ($vcId == 4) {
                $harga = 50000;
            }

            $ps->price = $harga; // WARNING: tidak ada initial & default value (uninitialized variable)
            $ps->account_id = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate = $request->license_plate_number;
            $ps->lane = $request->lane;
            $ps->save();

            $ac->balance = $ac->balance - $harga;
            $ac->save();

            echo 'success';
        } else {
            echo 'failed';
        }
    }

...
```

## Step 5: Tambah Pelanggaran Saldo Kurang

```
use use Illuminate\Support\Facades\DB;

use App\Enforcement;
// untuk urutan use, gunakan best practice
...

    function receive(Request $request) {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        if ($ac != '') {
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }

            if ($vcId == 1) {
                $harga = 10000;
            }
            else if ($vcId == 2) {
                $harga = 25000;
            }
            else if ($vcId == 3) {
                $harga = 30000;
            }
            else if ($vcId == 4) {
                $harga = 50000;
            }

            $ps->price = $harga;
            $ps->account_id = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate = $request->license_plate_number;
            $ps->lane = $request->lane;
            $ps->save();

            // Handle requirement pelanggaran saldo kurang
            if ($ac->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]); // WARNING: hardcode & magic numbers
            }
            $ac->balance = $ac->balance - $harga;
            $ac->save();

            echo 'success';
        } else {

            echo 'failed';
        }
    }
    
...
```

## Step 5: Tambah Pelanggaran Tanpa IKE

```
...


    function receive(Request $request) {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        if ($ac != '') {
            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }

            if ($vcId == 1) {
                $harga = 10000;
            }
            else if ($vcId == 2) {
                $harga = 25000;
            }
            else if ($vcId == 3) {
                $harga = 30000;
            }
            else if ($vcId == 4) {
                $harga = 50000;
            }

            $ps->price = $harga;
            $ps->account_id = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate = $request->license_plate_number;
            $ps->lane = $request->lane;
            $ps->save();

            if ($ac->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]); // WARNING: hardcode & magic numbers
            }
            $ac->balance = $ac->balance - $harga;
            $ac->save();

            echo 'success';
        } else {     // Handle requirement pelanggaran tanpa IKE

            $vc = VehicleClass::where('code', $request->vehicle_class)->first();
            if ($vc != '') {
                $vcId = $vc->id;
            } else {
                $vcId = null;
            }

            if ($vcId == 1) {
                $harga = 10000;
            }
            else if ($vcId == 2) {
                $harga = 25000;
            }
            else if ($vcId == 3) {
                $harga = 30000;
            }
            else if ($vcId == 4) {
                $harga = 50000;
            }

            $ps->price = $harga;
            $ps->account_id = null;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate = $request->license_plate_number;
            $ps->lane = $request->lane;
            $ps->save();


            DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]); // WARNING: hardcode & magic numbers

            echo 'success';
        }
    }
    
...
```

- Coba test dan lihat di database hasilnya
 

## Checklist

Akhirnya kita berhasil membuat satu fitur, yaitu passage. Tetapi tidak terasa code yang kita buat semakin lama semakin kompleks. Kode ini biasa disebut spaghetti atau dirty (kotor). Mari kita periksa daftarnya: 

- Tidak diformat mengikuti standard (PSR-2)
- Melanggar prinsip SOLID:
    - Satu class/fungsi banyak tugas = low cohesion
    - Tight coupling, tidak dapat di-extend
    - Banyak hardcode dan magic numbers
    - dll
- WET (Write Everything Twice), tidak mengikuti prinsip DRY (Don't Repeat Yourself). Banyak duplikasi
- Tidak ada pattern, tidak teratur
- Sulit dibaca & dipahami
- Sulit ditest
- dll

## Refactor

Silakan refactor code ini menjadi clean mengikuti prinsip yang telah dijelaskan!


# "The Better Way"

Berikut adalah jurus-jurus / langkah-langkah untuk membuat kode menjadi lebih clean:

## Format (PSR-2)

- Format dengan mengikuti PSR-2 (gunakan plugin pada IDE / php-cs-fixer)
- Tambah komentar, kurang lebih seperti berikut:

```php
...

    /**
     * End point untuk menerima dan rating kendaraan yang melintas
     *
     * @param Request $request dengan key:
     * <ul>
     *   <li>account_number string nomor IKE</li>
     *   <li>vehicle_class  string, golongan kendaraan contoh 001, 002</li>
     *   <li>license_plate_number  string plat nomor, contoh B123C</li>
     *   <li>lane string lajur JBE yang dilalui contoh 1, 2</li>
     * </ul>
     * @return null
     */
    function receive(Request $request)
    {
     
...
```

## Get DRY

- Refactor code yang duplikat, WET menjadi DRY

```php
...

    function receive(Request $request)
    {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        $vc = VehicleClass::where('code', $request->vehicle_class)->first();
        if ($vc != '') {
            $vcId = $vc->id;
        } else {
            $vcId = null;
        }

        if ($vcId == 1) {
            $harga = 10000;
        } else if ($vcId == 2) {
            $harga = 25000;
        } else if ($vcId == 3) {
            $harga = 30000;
        } else if ($vcId == 4) {
            $harga = 50000;
        }
        if ($ac != '') {

            $ps->price            = $harga;
            $ps->account_id       = $ac->id;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate    = $request->license_plate_number;
            $ps->lane             = $request->lane;
            $ps->save();

            if ($ac->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 1,
                    'fine' => 50000, 'status_id' => 1]); // WARNING: hardcode & magic numbers
            }
            $ac->balance = $ac->balance - $harga;
            $ac->save();

            echo 'success';
        } else {

            $ps->price            = $harga;
            $ps->account_id       = null;
            $ps->vehicle_class_id = $vcId;
            $ps->license_plate    = $request->license_plate_number;
            $ps->lane             = $request->lane;
            $ps->save();


            DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]); // WARNING: hardcode & magic numbers

            echo 'success';
        }
    }

...
```

## DRYer

- Minimalkan duplikasi
- Gunakan ternary operator untuk menyingkat code agar tidak terlalu panjang

```php
    function receive(Request $request)
    {
        $ps = new Passage();

        $ac = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        $vc = VehicleClass::where('code', $request->vehicle_class)->first();
        $vcId = ($vc != '') ? $vc->id : null;

        if ($vcId == 1) {
            $harga = 10000;
        } else if ($vcId == 2) {
            $harga = 25000;
        } else if ($vcId == 3) {
            $harga = 30000;
        } else if ($vcId == 4) {
            $harga = 50000;
        }
        $ps->price            = $harga;
        $ps->vehicle_class_id = $vcId;
        $ps->license_plate    = $request->license_plate_number;
        $ps->lane             = $request->lane;
        
        if ($ac != '') {
            $ps->account_id       = $ac->id;    
            $ps->save();

            if ($ac->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]); 
            }
            $ac->balance = $ac->balance - $harga;
            $ac->save();

            echo 'success';
        } else {
            $ps->account_id       = null;
            $ps->save();

            DB::table('enforcements')->insert(['passage_id' => $ps->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]); 

            echo 'success';
        }
    }
```

## Fix Naming

- Refactor nama yang kurang deskriptif

```php
...

    function receive(Request $request)
    {
        $passage = new Passage();

        $account = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        $vehicleClass = VehicleClass::where('code', $request->vehicle_class)->first();
        $vehicleClassId = ($vehicleClass != '') ? $vehicleClass->id : null;

        if ($vehicleClassId == 1) {
            $harga = 10000;
        } else if ($vehicleClassId == 2) {
            $harga = 25000;
        } else if ($vehicleClassId == 3) {
            $harga = 30000;
        } else if ($vehicleClassId == 4) {
            $harga = 50000;
        }
        $passage->price            = $harga;
        $passage->vehicle_class_id = $vehicleClassId;
        $passage->license_plate    = $request->license_plate_number;
        $passage->lane             = $request->lane;

        if ($account != '') {
            $passage->account_id       = $account->id;
            $passage->save();

            if ($account->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]); // WARNING: hardcode & magic numbers
            }
            $account->balance = $account->balance - $harga;
            $account->save();

            echo 'success';
        } else {
            $passage->account_id       = null;
            $passage->save();

            DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]); // WARNING: hardcode & magic numbers

            echo 'success';
        }
    }
    
...
```

## Follow SOLID

- Coba ikuti SRP, OCP, LSP, ISP, dan DIP
- Kita coba analisa tanggung jawab yang ada di class / function ini: 
    - Perhitungan biaya seharusnya menjadi tanggung jawab service terpisah, contoh Billing
    - Kita coba pisahkan tanggung jawab tersebut, ekstrak code yang melakukan rating ke class tersendiri

```php
...
use App\Services\Billing;
...

    function receive(Request $request)
    {
        $passage = new Passage();

        $account = Account::where('status_id', 1)->where('number', $request->account_number)->first();

        $vehicleClass   = VehicleClass::where('code', $request->vehicle_class)->first();
        $vehicleClassId = ($vehicleClass != '') ? $vehicleClass->id : null;

        // ekstrak code perhitungan biaya / rate
        $billing = new Billing();
        $harga = $billing->rate($request);

        $passage->price            = $harga;
        $passage->vehicle_class_id = $vehicleClassId;
        $passage->license_plate    = $request->license_plate_number;
        $passage->lane             = $request->lane;

        if ($account != '') {
            $passage->account_id       = $account->id;
            $passage->save();

            if ($account->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]); // WARNING: hardcode & magic numbers
            }
            $account->balance = $account->balance - $harga;
            $account->save();

            echo 'success';
        } else {
            $passage->account_id       = null;
            $passage->save();

            DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]); // WARNING: hardcode & magic numbers

            echo 'success';
        }
    }
    
...    
```

- Buat file `App\Services\Billing.php`
    
```php

<?php

namespace App\Services;

class Billing
{

    /**
     * Hitung vehicle class menjadi rupiah
     *
     * @param Passage $passage
     */
    function rate($passage)
    {
        $harga = null;
        if ($passage->vehicle_class == 1) {
            $harga = 10000;
        } else if ($passage->vehicle_class == 2) {
            $harga = 25000;
        } else if ($passage->vehicle_class == 3) {
            $harga = 30000;
        } else if ($passage->vehicle_class == 4) {
            $harga = 50000;
        }
        return $harga;
    }
}
```

- Coba test hasilnya
- Pisahkan juga dengan enforcement

```php
<?php

namespace App\Services;

use DB;
/**
 * Enforcement service
 *
 */
class Enforcement
{
    /**
     * Check and save if enforced
     * 
     * @param Passage $passage
     * @param Account $account
     * @return boolean
     */
    function submit($passage, $account = null)
    {
        if ($account == null) {
            DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]);
        } else {
            if ($account->balance < $passage->price) {
                DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]);
            } else {
                return false;
            }
        }
        return false;
    }

}
```

- Gunakan repository pattern untuk memisahkan concern terhadap akses data
- Gunakan dependency injection agar low coupling dan memudahkan testing
- Buat interface repository `App\Repositories\AccountRepositoryInterface.php` dan implementasinya `App\Repositories\AccountRepository.php`

```php
<?php

namespace App\Repositories;

interface AccountRepositoryInterface
{

    function getActiveByNumber($number);
}

```

```php
<?php

namespace App\Repositories;

use App\Repositories\AccountRepositoryInterface;
use App\Account;

class AccountRepository implements AccountRepositoryInterface
{

    function getActiveByNumber($number)
    {
        return Account::where('number', $number)->where('status_id', 1)->first();
    }
}
```

- Registrasikan service / class yang telah dibuat
- Buka `app/ServiceProviders/AppServiceProviders.php`, tambahkan:

```php
...

    public function register()
    {
        ...
        
        $this->app->bind(
            \App\Repositories\AccountRepositoryInterface::class, \App\Repositories\AccountRepository::class
        );
        
        ...
    }
    
...
```
- Ulangi untuk model yang lain (sudah disediakan)
- Pada controller, gunakan interface sebagai constructor parameter

```php
...
use App\Repositories\AccountRepositoryInterface;
use App\Repositories\EnforcementRepositoryInterface;
use App\Repositories\PassageRepositoryInterface;
use App\Repositories\VehicleClassRepositoryInterface;
...
    function __construct(
    AccountRepositoryInterface $accountRepo, EnforcementRepositoryInterface $enforcementRepo,
    PassageRepositoryInterface $passageRepo, VehicleClassRepositoryInterface $vehicleClassRepo
    )
    {
        $this->accountRepo      = $accountRepo;
        $this->enforcementRepo  = $enforcementRepo;
        $this->passageRepo      = $passageRepo;
        $this->vehicleClassRepo = $vehicleClassRepo;
    }
...

```

- pada function receive, gunakan repo tersebut

```php
...

    function receive(Request $request)
    {
        $passage = new Passage();

        $account = $this->accountRepo->getActiveByNumber($request->account_number);

        $vehicleClass   = $this->vehicleClassRepo->getByCode($request->vehicle_class);
        $vehicleClassId = ($vehicleClass != '') ? $vehicleClass->id : null;

        $billing = new Billing();
        $harga   = $billing->rate($request);

        $passage->price            = $harga;
        $passage->vehicle_class_id = $vehicleClassId;
        $passage->license_plate    = $request->license_plate_number;
        $passage->lane             = $request->lane;

        if ($account != '') {
            $passage->account_id = $account->id;
            $passage->save();

            if ($account->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]);
            }
            $account->balance = $account->balance - $harga;
            $account->save();

            echo 'success';
        } else {
            $passage->account_id = null;
            $passage->save();

            DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]);

            echo 'success';
        }
    }
    
...
```

- Test hasilnya
- Selanjutnya, coba pisahkan fitur Enforcement seperti memisahkan Billing
- Buat file `App\Services\Enforcement.php`

```php
<?php

namespace App\Services;

use DB;
/**
 * Enforcement service
 *
 */
class Enforcement
{
    /**
     * Check and save if enforced
     *
     * @param Passage $passage
     * @param Account $account
     * @return boolean
     */
    function submit($passage, $account = null)
    {
        if ($account == null) {
            DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]);
        } else {
            if ($account->balance < $harga) {
                DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]);
            } else {
                return false;
            }
        }
        return false;
    }

}

```

- Refactor controller 
```php
...
    function receive(Request $request)
    {
        $passage = new Passage();

        $account = $this->accountRepo->getActiveByNumber($request->account_number);

        $vehicleClass   = $this->vehicleClassRepo->getByCode($request->vehicle_class);
        $vehicleClassId = ($vehicleClass != '') ? $vehicleClass->id : null;

        $billing = new Billing();
        $harga   = $billing->rate($request);

        $passage->price            = $harga;
        $passage->vehicle_class_id = $vehicleClassId;
        $passage->license_plate    = $request->license_plate_number;
        $passage->lane             = $request->lane;
        $passage->account_id       = $account != '' ? $account->id : null;
        $passage->save();

        $enforcement = new Enforcement();
        $enforcement->submit($passage, $account);

        if ($account != '') {
            $account->balance = $account->balance - $harga;
            $account->save();
        }

        echo 'success';
    }
...    
```    
- Sampai saat ini controller kita sudah jauh lebih rapi. Fungsionalitas yang terkait bisnis pun sudah terpisah sehingga lebih mudah ditest.
- Silakan gunakan jurus-jurus yang sudah dipelajari untuk lebih membersihkan lagi

## Focus on Billing

- Agar billing lebih SOLID dan mudah ditest, maka kita refactor class Billing
- Remove hardcode, get from repository, use DI

```php
<?php

namespace App\Services;

use App\Repositories\RateRepositoryInterface;

/**
 * Billing service
 *
 */
class Billing
{
    protected $rateRepo;
    protected $rates;

    function __construct(RateRepositoryInterface $rateRepo)
    {
        $this->rateRepo = $rateRepo;
        $this->rates    = $this->rateRepo->all();
    }

    /**
     * Hitung vehicle class menjadi rupiah
     *
     * @param Passage $passage
     */
    function rate($passage)
    {
        foreach ($this->rates as $rate) {
            if ((int) $passage->vehicle_class === (int) $rate->vehicle_class_id) {
                return $rate->price; // return first found
            }
        }
        return null;
    }
}
```

- Controller menjadi:

```php
...
    function receive(Request $request)
    {
        $passage = new Passage();

        $account = $this->accountRepo->getActiveByNumber($request->account_number);

        $vehicleClass   = $this->vehicleClassRepo->getByCode($request->vehicle_class);
        $vehicleClassId = ($vehicleClass != '') ? $vehicleClass->id : null;

        $billing = app(Billing::class);
        $harga   = $billing->rate($request);

        $passage->price            = $harga;
        $passage->vehicle_class_id = $vehicleClassId;
        $passage->license_plate    = $request->license_plate_number;
        $passage->lane             = $request->lane;
        $passage->account_id       = $account != '' ? $account->id : null;
        $passage->save();

        $enforcement = new Enforcement();
        $enforcement->submit($passage, $account);

        if ($account != '') {
            $account->balance = $account->balance - $harga;
            $account->save();
        }

        echo 'success';
    }
...    
```

- Jangan lupa untuk me-refactor magic numbers
- Catatan:
    - Masih banyak ruang untuk perbaikan, contoh: Jika ada perubahan pada vendor roadside, maka spesifikasi format data yang diterima kemungkinan akan berubah. Ini bisa diatasi contohnya dengan menggunakan 'anti-corruption layer', atau memasang adapter pada layer yang berhadapan dengan roadside. 
    - Code ini juga belum optimal pada prinsip ISP yaitu jika signature pada *RepositoryInterface berubah, maka harus mengubah semua class yang meng-implementnya. Solusinya dengan merencanakan Interface dengan baik sebelum melakukan develop source code agar software mudah di-maintain.  
    
# Conclusion

- Silakan explore lebih jauh lagi mengenai clean code dan architecture, karena masih banyak aspek yang tidak terbahas dalam workshop singkat ini.
- Practice 1 merupakan pondasi agar source code dapat dilakukan unit testing, karena source code awal sebelum refactoring akan sulit bahkan hamnpir tidak dapat ditest. Untuk itu mudah-mudahan temen-temen memahami Practice 1 ini dengan baik, sehingga memudahkan untuk Practice selanjutnya yaitu: unit testing. Sampai jumpa di session berikutnya.
